import datetime
from unittest import mock
from django.conf import settings
import os
import io
from django.test.client import encode_multipart, RequestFactory
from rest_framework.test import APIClient
from account.models import User
from monitoring.models import Proyek
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File
from django.db import transaction
from django.test import TestCase
from rest_framework.test import RequestsClient

from pengembalian_uang.models import LaporanPengembalianUang
from pengembalian_uang.serializers import LaporanPengembalianUangSerializer
from io import BytesIO
from PIL import Image
from django.core.files.images import ImageFile
from django.test.client import MULTIPART_CONTENT


class LaporanPengembalianUangModelTest(TestCase):
    """Defines the test suite for the laporan pengembalian uang model."""

    def setUp(self):
        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()
        self.laporan_obj = LaporanPengembalianUang(
            judul="laporan",
            deskripsi="sisa uang pembangunan masjid", 
            bukti_foto="foto", 
            proyek=self.proyek_obj, 
            user=self.user_obj
        )

    def test_create_new_laporan_pengembalian_uang_object_count_incremented(self):
        current_laporan_count = LaporanPengembalianUang.objects.count()
        self.laporan_obj.save()
        new_laporan_count = LaporanPengembalianUang.objects.count()
        self.assertNotEqual(current_laporan_count, new_laporan_count)

    def test_create_new_laporan_pengembalian_uang_data_is_saved(self):
        laporan = LaporanPengembalianUang.objects.create(
            judul="laporan", 
            deskripsi="sisa dana", 
            bukti_foto="foto", 
            proyek=self.proyek_obj, 
            user=self.user_obj
        )
        pk_laporan = laporan.pk
        new_laporan = LaporanPengembalianUang.objects.get(pk=pk_laporan)
        self.assertEqual(new_laporan.judul, "laporan")
        self.assertEqual(new_laporan.deskripsi, "sisa dana")
        self.assertEqual(new_laporan.bukti_foto, "foto")

    def test_when_input_invalid_should_not_create_laporan_pengembalian_uang(self):
        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()
        laporan_data = {
            "id": 1,
            "judul": "laporan mingguan",
            "deskripsi": "sisa uang pembangunan masjid",
            "bukti_foto": 1,
            "proyek": self.proyek_obj,
            "user": self.user_obj,
        }
        try:
            invalid_laporan = LaporanPengembalianUang(**laporan_data)
            with transaction.atomic():
                invalid_laporan.save()
        except AttributeError:
            self.assertEqual(LaporanPengembalianUang.objects.count(), 0)


class LaporanPengembalianUangSerializerTest(TestCase):
    def setUp(self):
        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()
        self.laporan_data = {
            "id": 1,
            "judul": "laporan mingguan",
            "deskripsi": "sisa uang pembangunan masjid",
            "bukti_foto": "/foto",
            "proyek": self.proyek_obj,
            "user": self.user_obj,
        }
        self.laporan_obj = LaporanPengembalianUang(**self.laporan_data)
        self.laporan_serializer = LaporanPengembalianUangSerializer(
            instance=self.laporan_obj)

    def test_laporan_serialier_contains_field(self):
        data = self.laporan_serializer.data
        self.assertEqual(set(data.keys()), set(
            ["id", "judul", "deskripsi", "bukti_foto","user", "user_name","proyek",'terakhir_diupdate']))


class RESTAPILaporanPengembalianUangTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/laporan_pengembalian_uang"
        self.url_detail = "http://localhost:8000/laporan_pengembalian_uang/1"
        self.staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }
        User.objects.create_user(**self.staf_data)
        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(self.file, 'png')
        self.file.name = 'test.png'
        self.file.seek(0)

        self.laporan_data = {
            "id": 1,
            "judul": "laporan mingguan",
            "deskripsi": "sisa uang pembangunan masjid",
            "bukti_foto": "/test.png",
            "proyek": self.proyek_obj
        }
        LaporanPengembalianUang(**self.laporan_data).save()
        response_token = self.client.post("http://localhost:8000/account/login", {
                                          'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']
        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }
        self.result_data = {
            "id": 1,
            "judul": "laporan mingguan",
            "deskripsi": "sisa uang pembangunan masjid",
            "bukti_foto": "/media/test.png",
            "proyek": 1,
            "user": 1
        }

    def test_url_laporan_pengembalian_uang_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_laporan_pengembalian_uang_return_json_laporan_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()["data"]), 1)

    def test_post_laporan_pengembalian_uang_return_json_that_sended(self):
        data = {
            "id": 1,
            "judul": "laporan mingguan",
            "deskripsi": "sisa uang pembangunan masjid",
            "bukti_foto": "/test.png",
            "proyek": 1,
            "user": 1
        }
        response = self.client.post(
            self.url_index, data, headers=self.auth_token,
            files={"bukti_foto": self.file})
        self.assertEqual(response.json()["data"][0]["judul"], "laporan mingguan")
        self.assertEqual(
            response.json()["data"][0]["deskripsi"], "sisa uang pembangunan masjid")
        self.assertEqual(response.json()["data"][0]["proyek"], 1)
        self.assertEqual(response.json()["data"][0]["user"], 1)

    def test_get_laporan_pengembalian_uang_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["id"], 1)
        self.assertEqual(response.json()[0]["judul"], "laporan mingguan")
        self.assertEqual(response.json()[0]["deskripsi"], "sisa uang pembangunan masjid")

    def test_get_laporan_pengembalian_uang_by_project_id_return_json(self):
        response = self.client.get(
            self.url_index + "?project_id=1", 
            headers=self.auth_token
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()["data"]), 1)
        self.assertEqual(response.json()["data"][0]["proyek"], 1)

    def test_delete_laporan_pengembalian_uang_by_id_return_204(self):
        response = self.client.delete(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

    def test_put_laporan_pengembalian_uang_should_change_values(self):

        put_data = {
            "judul": "laporan akhir",
            "deskripsi": "sisa uang tambahan",
            "proyek": 1,
            "user": 1
        }

        response = self.client.put(
            self.url_detail, data=put_data, headers=self.auth_token,
            files={"bukti_foto": self.file})

        self.assertEqual(response.json()["judul"], "laporan akhir")
        self.assertEqual(response.json()["deskripsi"], "sisa uang tambahan")
        self.assertEqual(response.json()["proyek"], 1)

    def test_put_laporan_pengembalian_uang_with_invalid_input_return_400(self):

        put_data = {
            "judul": "laporan akhir",
            "deskripsi": "sisa uang tambahan",
            "proyek": "test",
            "user": "test"
        }

        response = self.client.put(
            self.url_detail, data=put_data, headers=self.auth_token,
            files={"bukti_foto": self.file})

        self.assertEqual(response.status_code, 400)

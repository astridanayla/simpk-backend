from django.db import models
from account.models import User
from monitoring.models import Proyek

class LaporanPengembalianUang(models.Model):
    """Laporan Pengembalian Uang Entity Fields"""

    judul = models.CharField(max_length=30)
    deskripsi = models.TextField()
    bukti_foto = models.ImageField(
        upload_to='pengembalian_uang/static/pengembalian_uang')
    user = models.ForeignKey(User, on_delete=models.PROTECT, default=1)
    proyek = models.ForeignKey(Proyek, to_field='project_id', on_delete=models.PROTECT)
    terakhir_diupdate = models.DateTimeField(auto_now=True)
    
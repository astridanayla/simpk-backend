from django.urls import path
from pengembalian_uang.core.views import IndexLaporanPengembalianUang, LaporanPengembalianUangDetail

urlpatterns = [
    path('', IndexLaporanPengembalianUang.as_view(),
         name="index_laporan_pengembalian_uang"),
    path('<primary_key>',
         LaporanPengembalianUangDetail.as_view(), name="laporan_pengembalian_uang_detail")
]

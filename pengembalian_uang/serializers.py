from rest_framework import serializers
from .models import LaporanPengembalianUang


class LaporanPengembalianUangSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField(source="user.nama_lengkap", read_only=True)
    class Meta:
        model = LaporanPengembalianUang
        fields = (
            'id',
            'judul',
            'deskripsi',
            'user',
            'user_name',
            'bukti_foto',
            'proyek',
            'terakhir_diupdate',
        )

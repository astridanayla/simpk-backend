from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from account.permissions import ProfessionalReadOnlyPermission
from pengembalian_uang.models import LaporanPengembalianUang
from pengembalian_uang.serializers import LaporanPengembalianUangSerializer


class IndexLaporanPengembalianUang(APIView):
    permission_classes = (IsAuthenticated, ProfessionalReadOnlyPermission)
    authentication_class = JSONWebTokenAuthentication
    parser_classes = (MultiPartParser,)

    def post(self, request):
        data = request.data
        data["user"] = request.user.id
        serializer = LaporanPengembalianUangSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        project_id = request.query_params.get("project_id")

        if project_id:
            laporan_list = LaporanPengembalianUang.objects.filter(
                proyek__pk=project_id)
        else:
            laporan_list = LaporanPengembalianUang.objects.all()

        serializer = LaporanPengembalianUangSerializer(laporan_list, many=True)
        response = {
            "status_code": status.HTTP_200_OK,
            "message": "Success",
            "data": serializer.data
        }
        return Response(response)


class LaporanPengembalianUangDetail(APIView):
    permission_classes = (IsAuthenticated, ProfessionalReadOnlyPermission)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, primary_key):
        laporan_obj = LaporanPengembalianUang.objects.filter(pk=primary_key)
        serializer = LaporanPengembalianUangSerializer(laporan_obj, many=True)
        return Response(serializer.data)

    def delete(self, request, primary_key):
        LaporanPengembalianUang.objects.filter(pk=primary_key).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, primary_key):
        laporan = LaporanPengembalianUang.objects.filter(
            pk=primary_key).first()
        serializer = LaporanPengembalianUangSerializer(
            laporan,
            data=request.data,
            partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

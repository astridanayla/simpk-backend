from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.http import urlencode
from .models import LaporanPengembalianUang

admin.site.register(LaporanPengembalianUang)
"""
@admin.register(LaporanPengembalianUang)
class LaporanPengembalianUangAdmin(admin.ModelAdmin):
    list_display = (
        "judul",
        "user_link",
        "proyek_link",
        "deskripsi",
    )
    list_filter = ("judul", "user","proyek",)
    search_fields = ("judul", "user__nama_lengkap","proyek__nama_proyek",)

    def user_link(self, obj):
        user_name = obj.user.nama_lengkap
        url = (
            reverse('admin:account_user_changelist')
            + "?"
            + urlencode({"nama_lengkap": f"{user_name}"})
        )

        return format_html('<a href="{}">{}</a>', url, user_name)

    user_link.short_description = "Pembuat"
    
    def proyek_link(self, obj):
        project_name = obj.proyek.nama_proyek
        url = (
            reverse('admin:monitoring_proyek_changelist')
            + "?"
            + urlencode({"nama_proyek": f"{project_name}"})
        )

        return format_html('<a href="{}">{}</a>', url, project_name)

    proyek_link.short_description = "Proyek"
"""

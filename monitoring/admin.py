from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html
from django.utils.http import urlencode
from .models import Laporan, Item, RealisasiItem, PoinAnalisa, Proyek, Lampiran, BuktiLaporan

admin.site.register(Laporan)
admin.site.register(Item)
admin.site.register(RealisasiItem)
admin.site.register(PoinAnalisa)
admin.site.register(Lampiran)
admin.site.register(BuktiLaporan)
admin.site.register(Proyek)

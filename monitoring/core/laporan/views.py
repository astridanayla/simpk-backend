from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from account.permissions import ProfessionalReadOnlyPermission
from monitoring.models import Laporan
from monitoring.serializers import LaporanSerializer


class IndexLaporan(APIView):
    permission_classes = (IsAuthenticated, ProfessionalReadOnlyPermission)
    authentication_class = JSONWebTokenAuthentication

    def post(self, request):
        to_be_post = request.data.copy()
        to_be_post["user"] = request.user.id
        serializer = LaporanSerializer(data=to_be_post)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        project_id = request.GET.get('proyek')
        if project_id:
            laporan_list = Laporan.objects.filter(proyek=project_id)
        else:
            laporan_list = Laporan.objects.all()

        serializer = LaporanSerializer(laporan_list, many=True)
        return Response(serializer.data)


class LaporanDetail(APIView):
    permission_classes = (IsAuthenticated, ProfessionalReadOnlyPermission)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, laporan_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk)
        serializer = LaporanSerializer(laporan_obj, many=True)
        return Response(serializer.data)

    def delete(self, request, laporan_pk):
        Laporan.objects.filter(pk=laporan_pk).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, laporan_pk):
        laporan = Laporan.objects.filter(pk=laporan_pk).first()
        serializer = LaporanSerializer(laporan, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

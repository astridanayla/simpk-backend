from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from simpk.bpkh_api import get_mitra_list

class IndexMitra(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request):

        user = request.user

        if user.role != "Mitra" or user.role != "Professional":
            mitra_list = get_mitra_list(request.user)
            return Response(status=status.HTTP_200_OK, data=mitra_list)

        data = {
            "message": "User role prohibited from using this API."
        }
        return Response(status=status.HTTP_403_FORBIDDEN, data=data)

class MitraDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, id_mitra):

        user = request.user
        if user.role == "Mitra" or user.role == "Professional":
            data = {
            "message": "User role prohibited from using this API."
            }

            return Response(status=status.HTTP_403_FORBIDDEN, data=data)

        mitra_list = get_mitra_list(request.user)
        int_id_mitra = int(id_mitra) - 1

        if int_id_mitra >= len(mitra_list) or int_id_mitra < 0:
            data = {
                "message": "Mitra with id " + id_mitra + " not found."
            }
            return Response(status=status.HTTP_204_NO_CONTENT, data=data)

        return Response(status=status.HTTP_200_OK, data=mitra_list[int_id_mitra])

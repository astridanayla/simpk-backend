from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from monitoring.models import BuktiLaporan, Laporan
from monitoring.serializers import BuktiLaporanSerializer


class IndexBuktiLaporan(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication
    parser_classes = (MultiPartParser,)

    def post(self, request, laporan_pk):
        to_be_post = request.data
        to_be_post["laporan"] = laporan_pk
        serializer = BuktiLaporanSerializer(data=to_be_post)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, laporan_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        bukti_laporan_list = BuktiLaporan.objects.filter(laporan=laporan_obj)
        serializer = BuktiLaporanSerializer(bukti_laporan_list, many=True)
        return Response(serializer.data)


class BuktiLaporanDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, laporan_pk, bukti_laporan_pk):
        laporan_obj = Laporan.objects.get(pk=laporan_pk)
        bukti_laporan_obj = BuktiLaporan.objects.filter(laporan=laporan_obj,
            pk=bukti_laporan_pk)
        serializer = BuktiLaporanSerializer(bukti_laporan_obj, many=True)
        return Response(serializer.data)

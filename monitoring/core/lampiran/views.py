from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from monitoring.models import Lampiran, Proyek
from monitoring.serializers import LampiranSerializer

class IndexLampiran(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication
    parser_classes = (MultiPartParser,)

    def post(self, request, proyek_pk):
        to_be_post = request.data.copy()
        to_be_post["user"] = request.user.id
        to_be_post["proyek"] = proyek_pk
        serializer = LampiranSerializer(data=to_be_post)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, proyek_pk):
        proyek_obj = Proyek.objects.filter(pk=proyek_pk).first()
        lampiran_list = Lampiran.objects.filter(proyek=proyek_obj)
        serializer = LampiranSerializer(lampiran_list, many=True)
        return Response(serializer.data)

class LampiranDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, proyek_pk, lampiran_pk):
        proyek_obj = Proyek.objects.filter(pk=proyek_pk).first()
        lampiran_obj = Lampiran.objects.filter(pk=lampiran_pk, proyek=proyek_obj)
        serializer = LampiranSerializer(lampiran_obj, many=True)
        return Response(serializer.data)

    def delete(self, request, proyek_pk, lampiran_pk):
        proyek_obj = Proyek.objects.filter(pk=proyek_pk).first()
        Lampiran.objects.filter(pk=lampiran_pk, proyek=proyek_obj).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, proyek_pk, lampiran_pk):
        proyek_obj = Proyek.objects.filter(pk=proyek_pk).first()
        lampiran = Lampiran.objects.filter(pk=lampiran_pk, proyek=proyek_obj).first()
        serializer = LampiranSerializer(lampiran, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

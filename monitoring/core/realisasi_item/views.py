from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from monitoring.models import Laporan, RealisasiItem
from monitoring.serializers import RealisasiItemSerializer

class IndexRealisasiItem(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, laporan_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        realisasi_item_list = RealisasiItem.objects.filter(laporan=laporan_obj)
        serializer = RealisasiItemSerializer(realisasi_item_list, many=True)
        return Response(serializer.data)

    def post(self, request, laporan_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        serializer = RealisasiItemSerializer(data=request.data)
        if serializer.is_valid():
            item = RealisasiItem.objects.create(**serializer.data, laporan=laporan_obj)
            serializer = RealisasiItemSerializer(item)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RealisasiItemDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, laporan_pk, realisasi_item_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        realisasi_item_obj = RealisasiItem.objects.filter(pk=realisasi_item_pk,
            laporan=laporan_obj)
        serializer = RealisasiItemSerializer(realisasi_item_obj, many=True)
        return Response(serializer.data)

    def delete(self, request, laporan_pk, realisasi_item_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        RealisasiItem.objects.filter(pk=realisasi_item_pk, laporan=laporan_obj).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, laporan_pk, realisasi_item_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        realisasi_item_obj = RealisasiItem.objects.filter(pk=realisasi_item_pk,
            laporan=laporan_obj).first()
        serializer = RealisasiItemSerializer(realisasi_item_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

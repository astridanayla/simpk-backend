from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from monitoring.models import Proyek
from monitoring.core.proyek.views import format_result
from simpk.bpkh_api import get_project_list


class SearchProyekView(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request):
        filter_kwargs = {}
        if request.GET.get('namaProyek'):
            filter_kwargs['keyword'] = request.GET.get('namaProyek')
        if request.GET.get('page'):
            filter_kwargs['page'] = request.GET.get('page')

        project_list = get_project_list(request.user, **filter_kwargs)

        data = []
        for proyek in project_list:
            query = Proyek.objects.filter(project_id=proyek["projectId"])
            if len(query) == 0:
                proyek_obj = Proyek(project_id=proyek["projectId"],
                                    koordinat_lokasi="",
                                    deskripsi="",
                                    progres=0,
                                    kesimpulan_monitoring="")
                proyek_obj.save()

                result = format_result(proyek, proyek_obj)
                data.append(result)

            else:
                result = format_result(proyek, query.first())
                data.append(result)
        return Response(status=status.HTTP_200_OK, data=data)

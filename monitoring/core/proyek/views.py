from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from monitoring.models import Proyek
from monitoring.serializers import ProyekDetailSerializer
from simpk.bpkh_api import get_project_list, get_project

class IndexProyek(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request):
        is_status_count = request.GET.get('status_count')
        if is_status_count:
            finished = Proyek.objects.filter(progres=100).count()
            running = Proyek.objects.filter(
                progres__gt=0, progres__lt=100).count()

            data = {
                "status_code": status.HTTP_200_OK,
                "message": "Success",
                "data": {
                    # "total": total,
                    "selesai": finished,
                    "berjalan": running,
                    # "belum_mulai": not_started,
                }
            }
            return Response(data, status=status.HTTP_200_OK)

        project_list = get_project_list(request.user)
        data = []
        for proyek in project_list:
            query = Proyek.objects.filter(project_id=proyek["projectId"])
            if len(query) == 0:
                proyek_obj = Proyek(project_id=proyek["projectId"],
                                    koordinat_lokasi="",
                                    deskripsi="",
                                    progres=0,
                                    kesimpulan_monitoring="")
                proyek_obj.save()

                result = format_result(proyek, proyek_obj)
                data.append(result)

            else:
                result = format_result(proyek, query.first())
                data.append(result)
        return Response(status=status.HTTP_200_OK, data=data)


class ProyekDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, proyek_pk):
        proyek_obj = Proyek.objects.get(project_id=proyek_pk)
        proyek_json = get_project(request.user, project_id=proyek_pk)
        result = format_result(proyek_json, proyek_obj)
        return Response(data=result)

    def put(self, request, proyek_pk):
        proyek_obj = Proyek.objects.get(project_id=proyek_pk)
        serializer = ProyekDetailSerializer(proyek_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            proyek_json = get_project(request.user, project_id=proyek_pk)
            result = format_result(proyek_json, proyek_obj)
            return Response(result, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def format_result(proyek_json, proyek_obj):
    proyek_obj_json = ProyekDetailSerializer(proyek_obj)
    result = {
        "nama_proyek": proyek_json["title"],
        "jenis_kegiatan": proyek_json["scope"],
        "nama_penerima": proyek_json["recipientName"],
        "daerah": proyek_json["region"],
        "alokasi_dana": proyek_json["preBudget"],
        "mitra": proyek_json["partnerName"],
        "status": proyek_json["status"],
    }

    result.update(proyek_obj_json.data)

    return result

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from monitoring.models import Laporan, PoinAnalisa
from monitoring.serializers import PoinAnalisaSerializer

class IndexPoinAnalisa(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, laporan_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        poin_analisa_list = PoinAnalisa.objects.filter(laporan=laporan_obj)
        serializer = PoinAnalisaSerializer(poin_analisa_list, many=True)
        return Response(serializer.data)

    def post(self, request, laporan_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        serializer = PoinAnalisaSerializer(data=request.data)
        if serializer.is_valid():
            poin = PoinAnalisa.objects.create(**serializer.data, laporan=laporan_obj)
            serializer = PoinAnalisaSerializer(poin)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PoinAnalisaDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, laporan_pk, poin_analisa_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        poin_analisa_obj = PoinAnalisa.objects.filter(pk=poin_analisa_pk,
            laporan=laporan_obj)
        serializer = PoinAnalisaSerializer(poin_analisa_obj, many=True)
        return Response(serializer.data)

    def delete(self, request, laporan_pk, poin_analisa_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        PoinAnalisa.objects.filter(pk=poin_analisa_pk, laporan=laporan_obj).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, laporan_pk, poin_analisa_pk):
        laporan_obj = Laporan.objects.filter(pk=laporan_pk).first()
        poin_analisa_obj = PoinAnalisa.objects.filter(pk=poin_analisa_pk,
            laporan=laporan_obj).first()
        serializer = PoinAnalisaSerializer(poin_analisa_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

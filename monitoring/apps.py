from django.apps import AppConfig

class MonitoringConfig(AppConfig):
    """Default Django App Config"""
    name = 'monitoring'

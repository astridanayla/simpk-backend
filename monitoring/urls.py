from django.urls import path
from monitoring.core.proyek.views import IndexProyek, ProyekDetail
from monitoring.core.laporan.views import IndexLaporan, LaporanDetail
from monitoring.core.item.views import IndexItem, ItemDetail
from monitoring.core.realisasi_item.views import IndexRealisasiItem, RealisasiItemDetail
from monitoring.core.poin_analisa.views import IndexPoinAnalisa, PoinAnalisaDetail
from monitoring.core.lampiran.views import IndexLampiran, LampiranDetail
from monitoring.core.bukti_laporan.views import IndexBuktiLaporan, BuktiLaporanDetail
from monitoring.core.mitra.views import IndexMitra, MitraDetail

urlpatterns = [
    path('proyek', IndexProyek.as_view(), name="index_proyek"),
    path('proyek/<proyek_pk>', ProyekDetail.as_view(), name="proyek_detail"),
    path('laporan', IndexLaporan.as_view(), name="index_laporan"),
    path('laporan/<laporan_pk>', LaporanDetail.as_view(), name="laporan_detail"),
    path('laporan/<laporan_pk>/item', IndexItem.as_view(), name="index_item"),
    path('laporan/<laporan_pk>/item/<item_pk>',
         ItemDetail.as_view(), name="item_detail"),
    path('laporan/<laporan_pk>/realisasi-item', IndexRealisasiItem.as_view(),
         name="index_realisasi_item"),
    path('laporan/<laporan_pk>/realisasi-item/<realisasi_item_pk>',
         RealisasiItemDetail.as_view(), name="realisasi_item_detail"),
    path('laporan/<laporan_pk>/poin-analisa', IndexPoinAnalisa.as_view(),
         name="index_poin_analisa"),
    path('laporan/<laporan_pk>/poin-analisa/<poin_analisa_pk>',
         PoinAnalisaDetail.as_view(), name="poin_analisa_detail"),
    path('laporan/<laporan_pk>/bukti-laporan', IndexBuktiLaporan.as_view(),
         name="index_bukti_laporan"),
    path('laporan/<laporan_pk>/bukti-laporan/<bukti_laporan_pk>',
         BuktiLaporanDetail.as_view(), name="bukti_laporan_detail"),
    path('proyek/<proyek_pk>/lampiran', IndexLampiran.as_view(),
         name="lampiran_laporan"),
    path('proyek/<proyek_pk>/lampiran/<lampiran_pk>',
         LampiranDetail.as_view(), name="lampiran_detail"),
    path('mitra', IndexMitra.as_view(), name="index_mitra"),
    path('mitra/<id_mitra>', MitraDetail.as_view(), name="mitra_detail"),
]

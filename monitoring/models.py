from django.db import models
from django.db.utils import OperationalError
from account.models import User


class Proyek(models.Model):
    """Proyek Entity Fields"""

    project_id = models.IntegerField(primary_key=True)
    koordinat_lokasi = models.TextField(blank=True)
    deskripsi = models.TextField(blank=True)
    progres = models.IntegerField()
    kesimpulan_monitoring = models.TextField(blank=True)

    def __str__(self):
        return str(self.project_id)

    def total_realisasi(self):
        try:
            return RealisasiItem.objects.filter(laporan__proyek=self).aggregate(
                models.Sum('realisasi'))['realisasi__sum']
        except OperationalError:  # IntegerOverflow
            return -1

    def jumlah_laporan(self):
        return self.laporan_set.count()


class Laporan(models.Model):
    """Laporan Entity Fields"""

    class JenisKegiatan(models.TextChoices):
        KEND = "kendaraan", "Kendaraan"
        SARANA = "sarana_prasarana", "Sarana dan Prasarana"
        KEGIATAN = "kegiatan", "Kegiatan"
        LAINNYA = "lainnya", "Lainnya"

    class MetodePelaksanaan(models.TextChoices):
        PEMERIKSAAN = "pemeriksaan", "Pemeriksaan"
        CEK_FISIK = "cek_fisik", "Cek Fisik"
        PIHAK_KETIGA = "laporan_pihak_ketiga", "Laporan Konsultan/pihak ketiga"
        LAINNYA = "lainnya", "Lainnya"

    class TahapanDicapai(models.TextChoices):
        PEMBAYARAN = "pembayaran", "Pembayaran"
        PENERIMAAN = "penerimaan", "Penerimaan"

    class Kesimpulan(models.TextChoices):
        SESUAI = "sesuai", "Sesuai"
        BELUM = "belum_sesuai", "Belum Sesuai"
        BELUM_SESUAI_LANJUT = "belum_sesuai_lanjut", "Belum Sesuai Lanjut"

    jenis_kegiatan = models.CharField(
        max_length=30, choices=JenisKegiatan.choices)
    nama_kegiatan = models.CharField(max_length=50)
    metode_pelaksanaan = models.CharField(
        max_length=30, choices=MetodePelaksanaan.choices)
    tgl_monitoring = models.DateField()
    periode = models.IntegerField()
    tahapan_dicapai = models.CharField(
        max_length=30, choices=TahapanDicapai.choices)
    kesimpulan = models.CharField(max_length=50, choices=Kesimpulan.choices)
    proyek = models.ForeignKey(Proyek, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)


class Perbedaan(models.TextChoices):
    YA = "ya", "Ya"
    TIDAK = "tidak", "Tidak"


class Item(models.Model):
    """Item Entity Fields"""

    nama_item = models.CharField(max_length=30)
    persetujuan = models.CharField(max_length=30)
    realisasi = models.CharField(max_length=30)
    perbedaan = models.CharField(max_length=30, choices=Perbedaan.choices)
    deskripsi_perbedaan = models.TextField()
    laporan = models.ForeignKey(Laporan, on_delete=models.CASCADE)


class RealisasiItem(models.Model):
    """Realisasi Item Entity Fields"""

    nama_item = models.CharField(max_length=30)
    persetujuan = models.IntegerField()
    realisasi = models.IntegerField()
    perbedaan = models.CharField(max_length=30, choices=Perbedaan.choices)
    deskripsi_perbedaan = models.TextField()
    laporan = models.ForeignKey(Laporan, on_delete=models.CASCADE)


class PoinAnalisa(models.Model):
    """Poin Analisa Entity Fields"""

    rekomendasi = models.TextField()
    masalah = models.TextField()
    laporan = models.ForeignKey(Laporan, on_delete=models.CASCADE)


class Lampiran(models.Model):
    """Lampiran Entity Fields"""

    nama_lampiran = models.CharField(max_length=50)
    jenis_lampiran = models.CharField(max_length=10)
    created_at = models.DateField(auto_now=True)
    lampiran_file = models.FileField()
    proyek = models.ForeignKey(
        Proyek, to_field='project_id', on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.PROTECT)


class BuktiLaporan(models.Model):
    """Bukti Laporan Entity Fields"""

    file = models.ImageField(
        upload_to='monitoring/static/monitoring_bukti')
    laporan = models.ForeignKey(Laporan, on_delete=models.CASCADE)

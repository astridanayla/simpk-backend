import datetime
from unittest import mock
from django.test import TestCase
from rest_framework.test import RequestsClient
from monitoring.models import Laporan, PoinAnalisa, Proyek
from monitoring.serializers import PoinAnalisaSerializer
from account.models import User


class PoinAnalisaModelTest(TestCase):
    """This class defines the test suite for the poin analisa model."""

    def setUp(self):
        """Define the test client and other test variables."""
        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=self.proyek_obj, user=user_obj)

        self.laporan_obj.save()

    def test_create_new_poin_analisa_object_count_incremented(self):
        """Test the item model when new object created, count should be incremented"""

        poin_analisa = PoinAnalisa(rekomendasi="Review ulang", masalah="Kurang dana",
                                   laporan=self.laporan_obj)

        current_poin_analisa_count = PoinAnalisa.objects.count()
        poin_analisa.save()

        new_poin_analisa_count = PoinAnalisa.objects.count()
        self.assertNotEqual(current_poin_analisa_count, new_poin_analisa_count)

    def test_when_create_invalid_poin_analisa_should_not_create_object(self):
        try:
            PoinAnalisa(rekomendasi="Review ulang", masalah="Kurang dana",
                        laporan="invalid laporan").save()
        except ValueError:
            self.assertEqual(PoinAnalisa.objects.count(), 0)


class PoinAnalisaSerializerTest(TestCase):
    def setUp(self):

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=self.proyek_obj, user=user_obj).save()

        self.poin_analisa_data = {
            "rekomendasi": "Review ulang",
            "masalah": "Kurang dana",
            "laporan": self.laporan_obj
        }

        self.poin_analisa = PoinAnalisa(**self.poin_analisa_data)

        self.poin_analisa_serializer = PoinAnalisaSerializer(
            instance=self.poin_analisa)

    def test_poin_analisa_serializer_contains_field(self):
        data = self.poin_analisa_serializer.data
        self.assertEqual(set(data.keys()), set(
            ["id", "rekomendasi", "masalah"]))


class RESTAPIPoinAnalisaTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/laporan/1/poin-analisa"
        self.url_detail = "http://localhost:8000/monitoring/laporan/1/poin-analisa/1"

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_data = {
            "jenis_kegiatan": "Pembangunan",
            "nama_kegiatan": "Mengawas",
            "metode_pelaksanaan": "Cek Fisik",
            "tgl_monitoring": "2020-04-12",
            "periode": 2,
            "tahapan_dicapai": "Pembayaran Kendaraan",
            "kesimpulan": "Sesuai",
            "proyek": self.proyek_obj,
            "user": user_obj
        }
        self.laporan_obj = Laporan(**self.laporan_data)
        self.laporan_obj.save()

        self.poin_analisa_data = {
            "rekomendasi": "Review ulang",
            "masalah": "Kurang dana",
            "laporan": self.laporan_obj
        }

        self.post_poin_analisa_test = {
            "rekomendasi": "Review ulang",
            "masalah": "Kurang dana"
        }

        PoinAnalisa(**self.poin_analisa_data).save()

        response_token = self.client.post("http://localhost:8000/account/login",
                                          {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_poin_analisa_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_poin_analisa_item_return_json_poin_analisa_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_poin_analisa_return_json_that_sended(self):
        response = self.client.post(self.url_index, self.post_poin_analisa_test,
                                    headers=self.auth_token)
        self.assertEqual(response.json()['rekomendasi'], "Review ulang")
        self.assertEqual(response.json()['masalah'], "Kurang dana")

    def test_get_poin_analisa_by_id_return_json(self):
        response = self.client.get(self.url_detail,
                                   headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]['rekomendasi'],
                         self.post_poin_analisa_test['rekomendasi'])

    def test_delete_poin_analisa_by_id_return_204(self):
        response = self.client.delete(self.url_detail,
                                      headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

    def test_put_poin_analisa_should_change_values(self):
        # Update value deskripsi_perbedaan and realisasi
        old_rekomendasi = self.post_poin_analisa_test["rekomendasi"]
        self.post_poin_analisa_test["rekomendasi"] = "Beli baru"

        response = self.client.put(self.url_detail, self.post_poin_analisa_test,
                                   headers=self.auth_token)
        self.assertEqual(response.json()["rekomendasi"], "Beli baru")
        self.assertNotEqual(response.json()["rekomendasi"], old_rekomendasi)

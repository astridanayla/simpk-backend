import datetime
from unittest import mock
from django.test import TestCase
from monitoring.models import Proyek, Lampiran
from django.db import transaction
from django.core.files.uploadedfile import SimpleUploadedFile
from monitoring.serializers import LampiranSerializer
from rest_framework.test import RequestsClient
from account.models import User


class LampiranModelTest(TestCase):
    """This class defines the test suite for the lampiran model"""

    def setUp(self):
        """Define the test client and other test variables"""

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)

        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        mock_file = SimpleUploadedFile(
            "mock_file.txt",
            # note the b in front of the string [bytes]
            b"these are the file contents!"
        )

        self.lampiran_obj = Lampiran(nama_lampiran="Test File",
                                     jenis_lampiran="berkas",
                                     lampiran_file=mock_file,
                                     proyek=self.proyek_obj,
                                     user=self.user_obj
                                     )

    def test_create_new_lampiran_object_count_incremented(self):
        current_lampiran_count = Lampiran.objects.count()
        self.lampiran_obj.save()
        new_lampiran_count = Lampiran.objects.count()
        self.assertNotEqual(current_lampiran_count, new_lampiran_count)

    def test_when_input_invalid_lampiran_data_should_not_create_lampiran(self):
        lampiran_data = {"stuff": "stuff"}

        try:
            invalid_lampiran = Lampiran(**lampiran_data)
        except TypeError:
            self.assertEqual(Lampiran.objects.count(), 0)


class LampiranSerializerTest(TestCase):

    def setUp(self):
        """Define the test client and other test variables"""

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)

        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        mock_file = SimpleUploadedFile(
            "mock_file.txt",
            # note the b in front of the string [bytes]
            b"these are the file contents!"
        )

        Lampiran(nama_lampiran="Test File",
                 jenis_lampiran="berkas",
                 lampiran_file=mock_file,
                 proyek=self.proyek_obj,
                 user=self.user_obj
                 ).save()

        self.lampiran_obj = Lampiran.objects.filter(pk=1).first()

        self.lampiran_serializer = LampiranSerializer(
            instance=self.lampiran_obj)

    def test_lampiran_serializer_contains_field(self):
        data = self.lampiran_serializer.data
        self.assertEqual(set(data.keys()), set(["nama_lampiran",
                                                "jenis_lampiran", "lampiran_file", "proyek", "user_name",
                                                "user", "created_at", "id"]))


class RESTAPILampiranTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/proyek/1/lampiran"
        self.url_detail = "http://localhost:8000/monitoring/proyek/1/lampiran/1"

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)

        self.user_obj = User.objects.filter(pk=1).first()

        proyek_obj = Proyek(project_id=1,
                            koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                            deskripsi="Lorem ipsum dolor sit amet",
                            progres="60")

        proyek_obj.save()

        self.mock_file = SimpleUploadedFile(
            "mock_file.txt",
            # note the b in front of the string [bytes]
            b"these are the file contents!"
        )

        self.lampiran_data = {
            "nama_lampiran": "Test File",
            "jenis_lampiran": "berkas",
            "lampiran_file": "/mock_file.txt",
            "proyek": proyek_obj,
            "user": self.user_obj
        }

        self.lampiran_obj = Lampiran(**self.lampiran_data)

        self.lampiran_obj.save()

        self.lampiran_data["proyek"] = 1
        self.lampiran_data["user"] = 1

        self.result_data = self.lampiran_data.copy()
        self.result_data["created_at"] = datetime.datetime.now().strftime(
            "%Y-%m-%d")
        self.result_data["user_name"] = 'Staf BPKH'
        self.result_data["lampiran_file"] = "/media/mock_file.txt"
        self.result_data["id"] = 1

        response_token = self.client.post("http://localhost:8000/account/login",
                                          {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_lampiran_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_lampiran_return_json_lampiran_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_lampiran_return_json_that_sended(self):
        response = self.client.post(self.url_index, self.lampiran_data,
                                    headers=self.auth_token, files={"lampiran_file": self.mock_file})
        self.result_data["lampiran_file"] = response.json()["lampiran_file"]
        self.result_data["id"] = 2
        self.assertEqual(response.json(), self.result_data)

    def test_post_lampiran_with_wrong_value_returns_http_400(self):
        self.lampiran_data["jenis_lampiran"] = "HALOHALOBANDUNGIBUKOTA"

        response = self.client.post(
            self.url_index, self.lampiran_data, headers=self.auth_token,
            files={"lampiran_file": self.mock_file})
        self.assertEqual(response.status_code, 400)

    def test_get_lampiran_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        self.result_data["id"] = 1
        self.assertEqual(response.json()[0], self.result_data)

    def test_delete_lampiran_by_id_return_204(self):
        response = self.client.delete(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

    def test_put_lampiran_should_change_name_value(self):
        self.lampiran_data["nama_lampiran"] = "test change"
        response = self.client.put(self.url_detail, self.lampiran_data,
                                   headers=self.auth_token, files={"lampiran_file": self.mock_file})
        self.assertEqual(response.json()["nama_lampiran"], "test change")

    def test_put_lampiran_with_wrong_format_returns_http_400(self):
        self.lampiran_data["nama_lampiran"] = b"Kabinet"
        response = self.client.put(
            self.url_detail, self.lampiran_data, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

from django.test import TestCase
from django.apps import apps
from monitoring.apps import MonitoringConfig

class UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(MonitoringConfig.name, "monitoring")
        self.assertEqual(apps.get_app_config("monitoring").name, "monitoring")

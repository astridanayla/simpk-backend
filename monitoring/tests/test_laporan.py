import datetime
from unittest import mock
from django.test import TestCase
from django.db import transaction
from rest_framework.test import RequestsClient
from monitoring.models import Laporan, Proyek
from monitoring.serializers import LaporanSerializer
from account.models import User


class LaporanModelTest(TestCase):
    """This class defines the test suite for the laporan model."""

    def setUp(self):
        """Define the test client and other test variables."""

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(
            jenis_kegiatan="kendaraan",
            nama_kegiatan="Mengawas",
            metode_pelaksanaan="cek_fisik",
            tgl_monitoring=datetime.date(2020, 4, 12),
            periode=2,
            tahapan_dicapai="pembayaran",
            kesimpulan="sesuai",
            proyek=self.proyek_obj,
            user=self.user_obj)

    def test_create_new_laporan_object_count_incremented(self):
        """Test the laporan model when new object created, count should be incremented"""

        current_laporan_count = Laporan.objects.count()
        self.laporan_obj.save()
        new_laporan_count = Laporan.objects.count()
        self.assertNotEqual(current_laporan_count, new_laporan_count)

    def test_when_input_invalid_laporan_data_should_not_create_laporan(self):
        laporan_data = {
            "jenis_kegiatan": "Pembangunan",
            "nama_kegiatan": "Mengawas",
            "metode_pelaksanaan": "Cek Fisik",
            "tgl_monitoring": "2020-04-12",
            "periode": "halo",
            "tahapan_dicapai": "Pembayaran Kendaraan",
            "kesimpulan": "Sesuai",
            "proyek": self.proyek_obj,
            "user": self.user_obj,
        }

        try:
            invalid_laporan = Laporan(**laporan_data)
            with transaction.atomic():
                invalid_laporan.save()
        except ValueError:
            self.assertEqual(Laporan.objects.count(), 0)


class LaporanSerializerTest(TestCase):
    def setUp(self):

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_data = {
            "id": 1,
            "jenis_kegiatan": "Pembangunan",
            "nama_kegiatan": "Mengawas",
            "metode_pelaksanaan": "Cek Fisik",
            "tgl_monitoring": "2020-04-12",
            "periode": 2,
            "tahapan_dicapai": "Pembayaran Kendaraan",
            "kesimpulan": "Sesuai",
            "proyek": self.proyek_obj,
            "user": self.user_obj
        }

        self.laporan_obj = Laporan(**self.laporan_data)

        self.laporan_serializer = LaporanSerializer(instance=self.laporan_obj)

    def test_laporan_serialier_contains_field(self):
        data = self.laporan_serializer.data
        self.assertEqual(set(data.keys()), set(["id", "jenis_kegiatan", "nama_kegiatan",
                                                "metode_pelaksanaan", "tgl_monitoring",
                                                "periode", "tahapan_dicapai",
                                                "kesimpulan", "proyek", "user", "user_name"]))


class RESTAPILaporanTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/laporan"
        self.url_detail = "http://localhost:8000/monitoring/laporan/1"

        self.staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**self.staf_data)

        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_data = {
            "id": 1,
            "jenis_kegiatan": "kendaraan",
            "nama_kegiatan": "Mengawas",
            "metode_pelaksanaan": "cek_fisik",
            "tgl_monitoring": "2020-04-12",
            "periode": 2,
            "tahapan_dicapai": "pembayaran",
            "kesimpulan": "sesuai",
            "proyek": self.proyek_obj,
            "user": self.user_obj
        }

        Laporan(**self.laporan_data).save()

        self.laporan_data["proyek"] = 1
        self.laporan_data["user"] = 1

        response_token = self.client.post(
            "http://localhost:8000/account/login",
            {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_laporan_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_laporan_return_json_laporan_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_laporan_return_json_that_sended(self):
        response = self.client.post(
            self.url_index, self.laporan_data, headers=self.auth_token)
        data = self.laporan_data.copy()
        data['id'] = 2
        data['user_name'] = self.user_obj.nama_lengkap
        self.assertEqual(response.json(), data)

    def test_post_laporan_with_wrong_format_returns_http_400(self):
        self.laporan_data["periode"] = "Kabinet"

        response = self.client.post(
            self.url_index, self.laporan_data, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

    def test_get_laporan_by_project_id(self):
        response = self.client.get(
            self.url_index + '?proyek=1', headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        response = self.client.get(
            self.url_index + '?proyek=2', headers=self.auth_token)
        self.assertEqual(len(response.json()), 0)

    def test_get_laporan_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        data = self.laporan_data.copy()
        data['user_name'] = self.user_obj.nama_lengkap
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0], data)

    def test_delete_laporan_by_id_return_204(self):
        response = self.client.delete(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

    def test_put_laporan_should_change_period_value(self):
        self.laporan_data["periode"] = 3
        response = self.client.put(
            self.url_detail, self.laporan_data, headers=self.auth_token)
        self.assertEqual(response.json()["periode"], 3)

    def test_put_laporan_with_wrong_format_returns_http_400(self):
        self.laporan_data["periode"] = "Kabinet"
        response = self.client.put(
            self.url_detail, self.laporan_data, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

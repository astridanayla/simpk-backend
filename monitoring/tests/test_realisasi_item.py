import datetime
from unittest import mock
from django.test import TestCase
from django.db import transaction
from rest_framework.test import RequestsClient
from monitoring.models import Laporan, RealisasiItem, Proyek
from monitoring.serializers import RealisasiItemSerializer
from account.models import User


class RealisasiItemModelTest(TestCase):
    """This class defines the test suite for the realisasi item model."""

    def setUp(self):
        """Define the test client and other test variables."""
        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=self.proyek_obj, user=user_obj)

        self.realisasi_obj = RealisasiItem(nama_item="Honda", persetujuan=10000, realisasi=2000,
                                           perbedaan="Ya", deskripsi_perbedaan="beda harga", laporan=self.laporan_obj)

    def test_create_new_realisasi_item_object_count_incremented(self):
        """Test the item model when new object created, count should be incremented"""
        self.laporan_obj.save()

        current_obj_count = RealisasiItem.objects.count()
        self.realisasi_obj.save()
        new_obj_count = RealisasiItem.objects.count()
        self.assertNotEqual(current_obj_count, new_obj_count)

    def test_when_input_invalid_realisasi_item_data_should_not_create_realisasi_item(self):
        realisasi_data = {
            "nama_item": "Merk",
            "persetujuan": 10000,
            "realisasi": "Yamaha",
            "perbedaan": 0,
            "deskripsi_perbedaan": "beda merk",
            "laporan": self.laporan_obj
        }

        self.laporan_obj.save()

        try:
            invalid_realisasi_item = RealisasiItem(**realisasi_data)
            with transaction.atomic():
                invalid_realisasi_item.save()
        except ValueError:
            self.assertEqual(RealisasiItem.objects.count(), 0)


class RealisasiItemSerializerTest(TestCase):
    def setUp(self):

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=self.proyek_obj, user=user_obj).save()

        self.realisasi_item_data = {
            "nama_item": "Merk",
            "persetujuan": 10000,
            "realisasi": 20000,
            "perbedaan": "Ya",
            "deskripsi_perbedaan": "beda harga",
            "laporan": self.laporan_obj
        }

        self.realisasi_item = RealisasiItem(**self.realisasi_item_data)

        self.realisasi_item_serializer = RealisasiItemSerializer(
            instance=self.realisasi_item)

    def test_realisasi_item_serializer_contains_field(self):
        data = self.realisasi_item_serializer.data
        self.assertEqual(set(data.keys()), set(["id", "nama_item", "persetujuan", "realisasi",
                                                "perbedaan", "deskripsi_perbedaan"]))


class RESTAPIRealisasiItemTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/laporan/1/realisasi-item"
        self.url_detail = "http://localhost:8000/monitoring/laporan/1/realisasi-item/1"

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        proyek_obj = Proyek(project_id=1,
                            koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                            deskripsi="Lorem ipsum dolor sit amet",
                            progres="60")

        proyek_obj.save()

        laporan_data = {
            "jenis_kegiatan": "Pembangunan",
            "nama_kegiatan": "Mengawas",
            "metode_pelaksanaan": "Cek Fisik",
            "tgl_monitoring": "2020-04-12",
            "periode": 2,
            "tahapan_dicapai": "Pembayaran Kendaraan",
            "kesimpulan": "Sesuai",
            "proyek": proyek_obj,
            "user": user_obj
        }
        laporan_obj = Laporan(**laporan_data)
        laporan_obj.save()

        realisasi_item_data = {
            "nama_item": "Merk",
            "persetujuan": 10000,
            "realisasi": 20000,
            "perbedaan": "ya",
            "deskripsi_perbedaan": "tidak ada perbedaan",
            "laporan": laporan_obj
        }

        self.post_realisasi_item_test = {
            "nama_item": "Merk",
            "persetujuan": 10000,
            "realisasi": 20000,
            "perbedaan": "ya",
            "deskripsi_perbedaan": "tidak ada perbedaan",
        }

        RealisasiItem(**realisasi_item_data).save()

        response_token = self.client.post("http://localhost:8000/account/login",
                                          {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_realisasi_item_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_realisasi_item_return_json_realisasi_item_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_realisasi_item_return_json_that_sended(self):
        response = self.client.post(self.url_index, self.post_realisasi_item_test,
                                    headers=self.auth_token)
        self.assertEqual(response.json()['nama_item'], "Merk")
        self.assertEqual(response.json()['realisasi'], 20000)

    def test_get_realisasi_item_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]['persetujuan'],
                         self.post_realisasi_item_test['persetujuan'])

    def test_delete_realisasi_item_by_id_return_204(self):
        response = self.client.delete(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

    def test_put_realisasi_item_should_change_values(self):
        # Update value deskripsi_perbedaan and realisasi
        old_deskripsi = self.post_realisasi_item_test["deskripsi_perbedaan"]
        old_realisasi = self.post_realisasi_item_test["realisasi"]
        self.post_realisasi_item_test["deskripsi_perbedaan"] = "beda harga di toko"
        self.post_realisasi_item_test["realisasi"] = 30000

        response = self.client.put(self.url_detail, self.post_realisasi_item_test,
                                   headers=self.auth_token)
        self.assertEqual(response.json()["realisasi"], 30000)
        self.assertNotEqual(
            response.json()["deskripsi_perbedaan"], old_deskripsi)
        self.assertNotEqual(response.json()["realisasi"], old_realisasi)

    def test_post_realisasi_item_with_invalid_data(self):
        self.post_realisasi_item_test["realisasi"] = "9juta"
        response = self.client.post(self.url_index, self.post_realisasi_item_test,
                                    headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

    def test_put_realisasi_item_with_invalid_data(self):
        self.post_realisasi_item_test["realisasi"] = "8jt"
        response = self.client.put(self.url_detail, self.post_realisasi_item_test,
                                   headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

from unittest import mock, skip
from django.test import TestCase
from rest_framework.test import RequestsClient
from monitoring.models import Proyek
from monitoring.serializers import ProyekDetailSerializer
from account.models import User

def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    if kwargs["url"] == 'http://nomor.bpkh.go.id/tracking-api/project/list':
        return MockResponse([{
            "projectId": "1",
            "submitCode": "2021-PDD-BA-03-002",
            "preSubmitDate": "2021-01-06",
            "submitDate": "null",
            "preTitle": "Permohonan Penambahan Sarana Pembangunan Kelas dan Asrama Putri Pesantren Modern dan Tahfizh Daru Ummah",
            "title": "null",
            "type": "Reguler",
            "scope": "Pendidikan dan Dakwah",
            "recipientName": "Yayasan Umat Mandiri Sejahtera",
            "region": "Kabupaten Tangerang, Banten",
            "partnerName": "null",
            "preBudget": "1524792000.00",
            "budget": "null",
            "status": "Penugasan Mitra Kemaslahatan"
            }], 200)
    elif kwargs["url"] == 'http://nomor.bpkh.go.id/tracking-api/proyek/detail':
        return MockResponse({
            "projectId": "1",
            "submitCode": "2021-PDD-BA-03-002",
            "preSubmitDate": "2021-01-06",
            "submitDate": "null",
            "preTitle": "Permohonan Penambahan Sarana Pembangunan Kelas dan Asrama Putri Pesantren Modern dan Tahfizh Daru Ummah",
            "title": "null",
            "type": "Reguler",
            "scope": "Pendidikan dan Dakwah",
            "recipientName": "Yayasan Umat Mandiri Sejahtera",
            "region": "Kabupaten Tangerang, Banten",
            "partnerName": "null",
            "preBudget": "1524792000.00",
            "budget": "null",
            "status": "Penugasan Mitra Kemaslahatan"
            }, 200)
    elif kwargs["url"] == 'http://nomor.bpkh.go.id/tracking-api/person':
         return MockResponse({"result": "OK", "data": {"personId": 1, "token": "asdf"}},200)

    return MockResponse(None, 404)


class ProyekModelTest(TestCase):
    """This class defines the test suite for the proyek model."""

    def setUp(self):
        """Define the test client and other test variables."""

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

    def test_create_new_proyek_object_count_incremented(self):
        """Test the proyek model when new object created, count should be incremented"""

        current_proyek_count = Proyek.objects.count()
        self.proyek_obj.save()
        new_proyek_count = Proyek.objects.count()
        self.assertNotEqual(current_proyek_count, new_proyek_count)

    def test_when_input_invalid_proyek_data_should_not_create_proyek(self):
        proyek_data = {
            "project_id": "haha",
            "koordinat_lokasi": "(-6.310016735496198, 106.82030167073002)",
            "deskripsi": "Lorem ipsum dolor sit amet",
            "progres": "60"
        }

        try:
            invalid_proyek = Proyek(**proyek_data)
        except ValueError:
            self.assertEqual(Proyek.objects.count(), 0)

    def test_str_proyek_return_pk_in_string(self):
        self.proyek_obj.save()
        self.proyek_obj = Proyek.objects.all().first()
        self.assertEqual(str(self.proyek_obj), "1")


class ProyekDetailSerializerTest(TestCase):
    def setUp(self):

        self.proyek_data = {
            "project_id": 1,
            "koordinat_lokasi": "(-6.310016735496198, 106.82030167073002)",
            "deskripsi": "Lorem ipsum dolor sit amet",
            "progres": "60"
        }

        self.proyek_obj = Proyek(**self.proyek_data)

        self.proyek_serializer = ProyekDetailSerializer(instance=self.proyek_obj)

    def test_proyek_serialier_contains_field(self):
        data = self.proyek_serializer.data
        self.assertEqual(set(data.keys()), set([
            "project_id", 'koordinat_lokasi', 'deskripsi', 'progres',
            'kesimpulan_monitoring','total_realisasi', 'jumlah_laporan']))


class RESTAPIProyekTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/proyek"
        self.url_detail = "http://localhost:8000/monitoring/proyek/1"

        self.staf_data = {
            'email': 'zona.ariemenda@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**self.staf_data)

        self.proyek_data = {
            "project_id": 1,
            "koordinat_lokasi": "(-6.310016735496198, 106.82030167073002)",
            "deskripsi": "Lorem ipsum dolor sit amet",
            "progres": "60"
        }

        self.proyek_obj = Proyek(**self.proyek_data)

        self.proyek_obj.save()

        response_token = self.client.post(
            "http://localhost:8000/account/login",
            {'email': 'zona.ariemenda@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    @skip
    @mock.patch('simpk.bpkh_api.requests.get', side_effect=mocked_requests_get)
    def test_url_proyek_api_is_exist(self, mocked_requests_get):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    @skip
    def test_get_proyek_return_json_proyek_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        # print(response)
        self.assertEqual(len(response.json()), 1)

    @skip
    def test_get_proyek_status_counts_is_exist(self):
        status_count_query = "status_count=true"
        response = self.client.get(
            self.url_index + f"?{status_count_query}",
            headers=self.auth_token
        )
        self.assertEqual(response.status_code, 200)

    @skip
    def test_get_proyek_status_counts_should_return_status_counts(self):
        status_count_query = "status_count=true"
        response = self.client.get(
            self.url_index + f"?{status_count_query}",
            headers=self.auth_token
        ).json()
        response_data = response["data"]
        self.assertEqual(response_data["total"], 1)
        self.assertEqual(response_data["selesai"], 0)
        self.assertEqual(response_data["berjalan"], 1)
        self.assertEqual(response_data["belum_mulai"], 0)

    @skip
    def test_get_distinct_regions_of_all_projects_exists(self):
        status_count_query = "unique_regions=true"
        response = self.client.get(
            self.url_index + f"?{status_count_query}",
            headers=self.auth_token
        )
        self.assertEqual(response.status_code, 200)

    @skip
    def test_get_distinct_regions_of_all_projects_should_return_distinct_regions(self):
        status_count_query = "unique_regions=true"
        response = self.client.get(
            self.url_index + f"?{status_count_query}",
            headers=self.auth_token
        ).json()
        response_data = response["data"]
        self.assertEqual(response_data[0], "Indonesia")
        self.assertEqual(response_data[1], "Kalimantan")

    @skip
    def test_get_proyek_status_count_by_region_is_exist(self):
        status_count_query = "status_count=true&region=kalimantan"
        response = self.client.get(
            self.url_index + f"?{status_count_query}",
            headers=self.auth_token
        )
        self.assertEqual(response.status_code, 200)

    @skip
    def test_get_proyek_status_count_by_region_returns_status_count_of_region(self):
        status_count_query = "status_count=true&region=kalimantan"
        response = self.client.get(
            self.url_index + f"?{status_count_query}",
            headers=self.auth_token
        ).json()
        response_data = response["data"]
        self.assertEqual(response_data["total"], 1)
        self.assertEqual(response_data["selesai"], 0)
        self.assertEqual(response_data["berjalan"], 1)
        self.assertEqual(response_data["belum_mulai"], 0)

    @skip
    def test_get_proyek_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.json(), self.result_data)

    @skip
    def test_put_proyek_should_change_progres_value(self):
        self.proyek_data["progres"] = 68
        response = self.client.put(
            self.url_detail, self.proyek_data, headers=self.auth_token)
        self.assertEqual(response.json()["progres"], 68)

    @skip
    def test_put_proyek_with_wrong_format_returns_http_400(self):
        self.proyek_data["progres"] = "Kabinet"
        response = self.client.put(
            self.url_detail, self.proyek_data, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

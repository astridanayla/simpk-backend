import mock
import os
import io

from PIL import Image
import datetime
from django.core.files import File
from django.test import TestCase
from rest_framework.test import RequestsClient
from monitoring.models import BuktiLaporan, Proyek, Laporan
from monitoring.serializers import BuktiLaporanSerializer
from account.models import User


class BuktiLaporanModelTest(TestCase):
    """This class defines the test suite for the bukti laporan model."""

    def setUp(self):
        """Define the test client and other test variables."""

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        proyek_obj = Proyek(project_id=1,
                            koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                            deskripsi="Lorem ipsum dolor sit amet",
                            progres="60")

        proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=proyek_obj, user=user_obj)
        self.laporan_obj.save()

        bukti_laporan_file_mock = mock.MagicMock(spec=File)
        bukti_laporan_file_mock.name = "bukti.png"

        self.bukti_laporan = BuktiLaporan(file=bukti_laporan_file_mock,
                                          laporan=self.laporan_obj)

    def test_create_new_bukti_laporan_object_count_incremented(self):
        """Test the bukti laporan model when new object created, count should be incremented"""

        current_bukti_count = BuktiLaporan.objects.count()
        self.bukti_laporan.save()
        new_bukti_count = BuktiLaporan.objects.count()
        self.assertNotEqual(current_bukti_count, new_bukti_count)


class BuktiLaporanSerializerTest(TestCase):
    def setUp(self):
        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        proyek_obj = Proyek(project_id=1,
                            koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                            deskripsi="Lorem ipsum dolor sit amet",
                            progres="60")

        proyek_obj.save()

        laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                              metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                              periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                              proyek=proyek_obj, user=user_obj)
        laporan_obj.save()

        bukti_laporan_file_mock = mock.MagicMock(spec=File)
        bukti_laporan_file_mock.name = "bukti.png"

        bukti_laporan_data = {
            "file": bukti_laporan_file_mock,
            "laporan": laporan_obj
        }

        bukti_laporan = BuktiLaporan(**bukti_laporan_data)
        self.bukti_laporan_serializer = BuktiLaporanSerializer(
            instance=bukti_laporan)

    def test_bukti_laporan_serializer_contains_field(self):
        data = self.bukti_laporan_serializer.data
        self.assertEqual(set(data.keys()), set(["file", "laporan"]))


class RESTAPIBuktiLaporanTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/laporan/1/bukti-laporan"
        self.url_detail = "http://localhost:8000/monitoring/laporan/1/bukti-laporan/1"

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        proyek_obj = Proyek(project_id=1,
                            koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                            deskripsi="Lorem ipsum dolor sit amet",
                            progres="60")

        proyek_obj.save()

        laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                              metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                              periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                              proyek=proyek_obj, user=user_obj)
        laporan_obj.save()

        self.bukti_laporan_file_mock = mock.MagicMock(spec=File)
        self.bukti_laporan_file_mock.name = "bukti.png"

        self.bukti_laporan_data = {
            "file": self.bukti_laporan_file_mock,
            "laporan": laporan_obj
        }

        BuktiLaporan(**self.bukti_laporan_data).save()

        self.bukti_laporan_data["laporan"] = 1

        response_token = self.client.post("http://localhost:8000/account/login",
                                          {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_bukti_laporan_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_bukti_laporan_return_json_bukti_laporan_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_bukti_laporan_return_json_that_sended(self):

        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)

        response = self.client.post(self.url_index, self.bukti_laporan_data,
                                    headers=self.auth_token, files={'file': file})

        self.assertEqual(response.json()['laporan'], 1)

    def test_get_bukti_laporan_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_bukti_laporan_with_invalid_data(self):
        self.bukti_laporan_data["file"] = "p"
        response = self.client.post(self.url_index, self.bukti_laporan_data,
                                    headers=self.auth_token)
        self.assertEqual(response.status_code, 415)

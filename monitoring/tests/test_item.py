import datetime
from unittest import mock
from django.test import TestCase
from rest_framework.test import RequestsClient
from monitoring.models import Laporan, Item, Proyek
from monitoring.serializers import ItemSerializer
from account.models import User


class ItemModelTest(TestCase):
    """This class defines the test suite for the item model."""

    def setUp(self):
        """Define the test client and other test variables."""

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=self.proyek_obj, user=self.user_obj)
        self.laporan_obj.save()

    def test_create_new_item_object_count_incremented(self):
        """Test the item model when new object created, count should be incremented"""

        item_obj = Item(nama_item="Merk", persetujuan="Honda", realisasi="Yamaha",
                        perbedaan="Ya", deskripsi_perbedaan="beda merk", laporan=self.laporan_obj)
        current_item_count = Item.objects.count()
        item_obj.save()
        new_item_count = Item.objects.count()
        self.assertNotEqual(current_item_count, new_item_count)

    def test_when_input_invalid_item_data_should_not_create_item(self):
        try:
            Item(nama_item="Merk", persetujuan="Honda", realisasi="Yamaha",
                 perbedaan=21029, deskripsi_perbedaan=200000, laporan="kucing").save()
        except ValueError:
            self.assertEqual(Item.objects.count(), 0)


class ItemSerializerTest(TestCase):
    def setUp(self):
        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.laporan_obj = Laporan(jenis_kegiatan="Pembangunan", nama_kegiatan="Mengawas",
                                   metode_pelaksanaan="Cek Fisik", tgl_monitoring=datetime.date(2020, 4, 12),
                                   periode=2, tahapan_dicapai="Pembayaran Kendaraan", kesimpulan="Sesuai",
                                   proyek=self.proyek_obj, user=self.user_obj).save()

        self.item_data = {
            "nama_item": "Merk",
            "persetujuan": "Honda",
            "realisasi": "Yamaha",
            "perbedaan": "Ya",
            "deskripsi_perbedaan": "beda merk",
            "laporan": self.laporan_obj
        }

        self.item = Item(**self.item_data)

        self.item_serializer = ItemSerializer(instance=self.item)

    def test_item_serializer_contains_field(self):
        data = self.item_serializer.data
        self.assertEqual(set(data.keys()), set(["id", "nama_item", "persetujuan", "realisasi",
                                                "perbedaan", "deskripsi_perbedaan"]))


class RESTAPIItemTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/monitoring/laporan/1/item"
        self.url_detail = "http://localhost:8000/monitoring/laporan/1/item/1"

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        user_obj = User.objects.filter(pk=1).first()

        proyek_obj = Proyek(project_id=1,
                            koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                            deskripsi="Lorem ipsum dolor sit amet",
                            progres="60")

        proyek_obj.save()

        laporan_data = {
            "jenis_kegiatan": "kendaraan",
            "nama_kegiatan": "Mengawas",
            "metode_pelaksanaan": "cek_fisik",
            "tgl_monitoring": "2020-04-12",
            "periode": 2,
            "tahapan_dicapai": "Pembayaran Kendaraan",
            "kesimpulan": "Sesuai",
            "proyek": proyek_obj,
            "user": user_obj
        }
        laporan_obj = Laporan(**laporan_data)
        laporan_obj.save()

        item_data = {
            "nama_item": "Merk",
            "persetujuan": "Honda",
            "realisasi": "Yamaha",
            "perbedaan": "ya",
            "deskripsi_perbedaan": "beda merk",
            "laporan": laporan_obj
        }

        self.post_item_test = {
            "id": 1,
            "nama_item": "Merk",
            "persetujuan": "Honda",
            "realisasi": "Yamaha",
            "perbedaan": "ya",
            "deskripsi_perbedaan": "beda merk"
        }

        Item(**item_data).save()

        response_token = self.client.post("http://localhost:8000/account/login",
                                          {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_item_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_get_item_return_json_item_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_item_return_json_that_sended(self):
        response = self.client.post(
            self.url_index, self.post_item_test, headers=self.auth_token)
        self.assertEqual(response.json()['nama_item'], "Merk")

    def test_get_item_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0], self.post_item_test)

    def test_delete_item_by_id_return_204(self):
        response = self.client.delete(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

    def test_put_item_should_change_values(self):
        # Update value deskripsi_perbedaan and realisasi
        old_deskripsi = self.post_item_test["deskripsi_perbedaan"]
        self.post_item_test["deskripsi_perbedaan"] = "beda merk dan jenis"
        self.post_item_test["realisasi"] = "Yamaha RX King"

        response = self.client.put(
            self.url_detail, self.post_item_test, headers=self.auth_token)
        self.assertEqual(response.json()["realisasi"], "Yamaha RX King")
        self.assertNotEqual(
            response.json()["deskripsi_perbedaan"], old_deskripsi)

    def test_post_item_with_invalid_data(self):
        self.post_item_test["realisasi"] = "qwertyuiopasdghjklxcvbnm,.12345"
        response = self.client.post(
            self.url_index, self.post_item_test, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

    def test_put_item_with_invalid_data(self):
        self.post_item_test["realisasi"] = "qwertyuiopasdghjklxcvbnm,.12345"
        response = self.client.put(self.url_detail,
                                   {"realisasi": "qwertyuiopasdghjklxcvbnm,.12345"}, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

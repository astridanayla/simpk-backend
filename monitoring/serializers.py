from rest_framework import serializers
from monitoring.models import (Laporan, Item, RealisasiItem, PoinAnalisa,
                               Proyek, BuktiLaporan, Lampiran)


class ItemSerializer(serializers.ModelSerializer):
    """Define the item serializer class"""

    class Meta:
        """Meta class for Item Model"""

        model = Item
        fields = ('id', 'nama_item', 'persetujuan', 'realisasi',
                  'perbedaan', 'deskripsi_perbedaan')


class RealisasiItemSerializer(serializers.ModelSerializer):
    """Define the realisasi item serializer class"""

    class Meta:
        """Meta class for Realisasi Item Model"""

        model = RealisasiItem
        fields = ('id', 'nama_item', 'persetujuan', 'realisasi',
                  'perbedaan', 'deskripsi_perbedaan')


class PoinAnalisaSerializer(serializers.ModelSerializer):
    """Define the poin analisa serializer class"""

    class Meta:
        """Meta class for Realisasi Item Model"""

        model = PoinAnalisa
        fields = ('id', 'masalah', 'rekomendasi')


class LaporanSerializer(serializers.ModelSerializer):
    """Define the laporan serializer class"""
    user_name = serializers.CharField(source='user.nama_lengkap', read_only=True)

    class Meta:
        """Meta class for Laporan Model"""

        model = Laporan
        fields = ('id', 'jenis_kegiatan', 'nama_kegiatan', 'metode_pelaksanaan',
                  'tgl_monitoring', 'periode', 'tahapan_dicapai', 'kesimpulan', 'proyek',
                  'user', 'user_name')


class ProyekDetailSerializer(serializers.ModelSerializer):
    """Define the proyek serializer class more detailed"""


    class Meta:
        """Meta class for proyek Model"""

        model = Proyek
        fields = ("project_id", 'koordinat_lokasi', 'deskripsi', 'progres',
                  'kesimpulan_monitoring',
                  'total_realisasi', 'jumlah_laporan')

class LampiranSerializer(serializers.ModelSerializer):
    """Define the Lampiran serializer class"""
    user_name = serializers.CharField(source='user.nama_lengkap', read_only=True)

    class Meta:
        """Meta class for Lampiran model"""

        model = Lampiran
        fields = ('id', 'nama_lampiran', 'jenis_lampiran','lampiran_file', 'user',
            'proyek', 'user_name', 'created_at')
        read_only_fields = ['created_at', 'id']

class BuktiLaporanSerializer(serializers.ModelSerializer):
    """Define the bukti laporan serializer class"""

    class Meta:
        """Meta class for bukti laporan Model"""
        model = BuktiLaporan
        fields = ('file', 'laporan')

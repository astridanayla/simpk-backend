#!/bin/bash

echo "Install dependencies"
apt-get update
apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib

# Don't forget to configure Database here
# sudo -u postgres psql
# create user <user-name> with password '<password>';
# ALTER ROLE <user-name> SET client_encoding TO 'utf8';
# ALTER ROLE <user-name> SET default_transaction_isolation TO 'read committed';
# ALTER ROLE <user-name> SET timezone TO 'Asia/Jakarta';
# create database <db-name> owner <user-name>;
# \q

echo "Create Virtual Env"
virtualenv env

echo "Switch to virtualenv"
source env/bin/activate

echo "Install Dependencies"
pip install -r requirements.txt

echo "Migrating seeds"
python manage.py migrate

#echo "Load seeds"
#python manage.py loaddata seed/*.json

echo "Collectstatis"
python manage.py collectstatic

echo "Switch permissions from root to www-data (apache)"
chown :www-data ~/simpk-backend/
chmod 777 ~/simpk-backend/

echo "Restarting apache2 webserver"
systemctl restart apache2

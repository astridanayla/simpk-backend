#!/bin/bash
echo "Migrating..."
python manage.py migrate
echo "Load data..."
python manage.py loaddata seed/*.json
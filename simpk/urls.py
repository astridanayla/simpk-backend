"""simpk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from monitoring.core.proyek.search import SearchProyekView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('monitoring/', include('monitoring.urls')),
    path('account/', include('account.urls')),
    path('laporan_pengembalian_uang/', include('pengembalian_uang.urls')),
    path('booking-app/', include('booking.urls')),
    path('search/proyek', SearchProyekView.as_view(), name="search_proyek"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

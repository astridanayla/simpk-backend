import json
import requests

from django.conf import settings

base_url = settings.BPKH_BASE_URL
auth_username = settings.BPKH_AUTH_USERNAME
auth_password = settings.BPKH_AUTH_PASSWORD


def get_person(email: str) -> dict:
    url = base_url + '/tracking-api/person'

    response = requests.get(
        url=url,
        data=json.dumps({
            'email': email
        }),
        auth=(auth_username, auth_password),
    )

    if response.status_code == 204:
        return {"result": 'ERROR'}
    return json.loads(response.content)


def refresh_user_token(user):
    url = base_url + '/tracking-api/person'

    response = requests.get(
        url=url,
        data=json.dumps({
            'email': user.get_bpkh_email(),
            'generateToken': 1,
        }),
        auth=(auth_username, auth_password),
    )

    # if user not found
    if response.status_code == 204:
        return user

    content = json.loads(response.content)

    if content['result'] == 'OK':
        user.bpkh_id = int(content['data']['personId'])
        user.bpkh_token = content['data']['token']
        user.save()
    else:
        print('ERROR get_person', content)

    return user


def requests_get(user, url: str, data, *args, **kwargs):
    """Handles invalid token"""

    if data is None:
        data = {}

    data.update({
        'personId': user.bpkh_id,
        'token': user.bpkh_token,
    })

    response = requests.get(
        url=url,
        data=json.dumps(data),
        auth=(auth_username, auth_password),
    )

    if response.status_code != 200:
        user = refresh_user_token(user)
        data.update({
            'personId': user.bpkh_id,
            'token': user.bpkh_token,
        })
        response = requests.get(
            url=url,
            data=json.dumps(data),
            auth=(auth_username, auth_password),
        )

    return response


def get_project_list(user, keyword=None, page=1):
    url = base_url + '/tracking-api/project/list'

    data = {
        'page': page,
    }
    if keyword is not None:
        data.update({'keyword': keyword})

    response = requests_get(
        user,
        url=url,
        data=data,
    )

    result = []

    # if result is empty
    if response.status_code == 204:
        return result

    content = json.loads(response.content)

    if content['result'] == 'OK':
        result = content['data']
    else:
        print('ERROR get_project_list', content)
    return result


def get_project(user, project_id):
    url = base_url + '/tracking-api/project/detail'

    response = requests_get(
        user,
        url=url,
        data={
            'projectId': project_id
        },
    )

    result = []
    content = json.loads(response.content)

    if content['result'] == 'OK':
        result = content['data']
    else:
        print('ERROR get_project', content)
    return result


def get_mitra_list(user):
    url = base_url + '/tracking-api/partner'

    response = requests_get(
        user,
        url=url
    )

    result = []
    content = json.loads(response.content)

    if content['result'] == 'OK':
        result = content['data']
    else:
        print('ERROR get_mitra_list', content)
    return result

"""
WSGI config for simpk project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
from dotenv import load_dotenv
from django.core.wsgi import get_wsgi_application

PRODUCTION = os.getenv('PRODUCTION') is not None
if PRODUCTION:
    project_folder = os.path.expanduser('~/simpk-backend')
    load_dotenv(os.path.join(project_folder, '.env'))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'simpk.settings')

application = get_wsgi_application()

# Sistem Informasi Manajemen Program Kemaslahatan - Backend

Staging server: [https://simpk-staging.herokuapp.com/](https://simpk-staging.herokuapp.com/)

## Pipeline Status

| Branch  | Pipeline                                                                                                                    | Coverage                                                                                                                    |
| ------- | --------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------- |
| master  | ![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-backend/badges/master/pipeline.svg)  | ![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-backend/badges/master/coverage.svg)  |
| staging | ![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-backend/badges/staging/pipeline.svg) | ![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-backend/badges/staging/coverage.svg) |

### Kebutuhan
* Python
* Python venv
* Semua package yang ada di requirements.txt

### Instalasi
Untuk instalasi proyek ini, jalankan command berikut:
```
git clone https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/DD/bpkh-simpk/simpk-backend.git
git fetch --all
```
Ganti working directory ke repository yang baru saja di clone dengan command:
```
cd simpk-backend
```
Buat sebuah folder environment dan jalankan environment untuk mengisolate instalasi kebutuhan dengan command:
```
python -m venv env
env\Scripts\activate
```
Lalu, install semua kebutuhan yang ada di dalam requirements.txt:
```
pip install -r requirements.txt
```
selesai

### Cara Menjalankan
Setelah selesai instalasi, jalankan perintah berikut untuk mulai menjalankan server di komputer anda.
```
py manage.py makemigrations
py manage.py migrate
py manage.py runserver
```

### Kontributor
- Abdurrafi Arief
- Astrida Nayla
- Mohamad Rifqy Zulkarnaen
- Muhammad Oktoluqman Fakhrianto
- Salman Ahmad Nurhoiriza

# Generated by Django 3.1.1 on 2021-07-05 21:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0003_auto_20210705_2343'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='mitra_id',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]

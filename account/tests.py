from unittest import mock

from django.test import TestCase
from django.apps import apps
from rest_framework.test import RequestsClient
from rest_framework import serializers

from account.apps import AccountConfig
from account.models import User


class UnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(AccountConfig.name, "account")
        self.assertEqual(apps.get_app_config("account").name, "account")


class UserTest(TestCase):
    def setUp(self):
        self.client = RequestsClient()
        self.user_data = {
            'email': 'mohamad.rifqy@bpkh.go.id',
            'password': 'dev-simpk-bpkh',
            'nama_lengkap': 'Mohamad Rifqy Zulkarnaen',
            'no_telp': '085688691865',
            'role': 'Professional'
        }

        self.user_data_put_test = {
            'email': 'mohamad.rifqy@bpkh.go.id',
            'nama_lengkap': 'Mohamad Rifqy Zulkarnaen',
            'no_telp': '085688691865',
        }

        self.admin_data = {
            'email': 'admin@bpkh.go.id',
            'password': 'adminbpkh',
            'nama_lengkap': 'Admin BPKH',
            'no_telp': '087444898211',
            'role': 'Admin'
        }

        self.staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        self.post_data = {
            'email': 'mohamad.rifqy@bpkh.go.id',
            'password': 'dev-simpk-bpkh'
        }

        User.objects.create_user(**self.user_data)
        User.objects.create_superuser(**self.admin_data)
        User.objects.create_user(**self.staf_data)

    @mock.patch('simpk.bpkh_api.requests.get')
    def test_get_token_for_authenticate(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        response = self.client.post(
            "http://localhost:8000/account/login", self.post_data)
        self.assertTrue(response.json()['token'])

    @mock.patch('simpk.bpkh_api.requests.get')
    def test_get_user_using_token(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        response_token = self.client.post("http://localhost:8000/account/login",
                                          self.post_data).json()['token']
        auth_token = {
            'Authorization': 'Bearer '+response_token
        }
        response = self.client.get(
            "http://localhost:8000/account/user", headers=auth_token)
        self.assertEqual(response.json()['data']
                         [0]['email'], self.post_data['email'])
        self.assertIn("Mohamad Rifqy Zulkarnaen", response.json()
                      ['data'][0]['nama_lengkap'])

    @mock.patch('simpk.bpkh_api.requests.get')
    def test_put_user_should_change_values(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        response_token = self.client.post(
            "http://localhost:8000/account/login",
            {'email': self.user_data['email'],
                'password': self.user_data['password']}
        ).json()['token']

        auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

        old_name = self.user_data['nama_lengkap']
        self.user_data_put_test['nama_lengkap'] = "Salman Ganteng"
        old_handphone_num = self.user_data['no_telp']
        self.user_data_put_test['no_telp'] = "081234567890"

        response = self.client.put(
            "http://localhost:8000/account/user",
            self.user_data_put_test,
            headers=auth_token
        ).json()

        self.assertTrue(response['success'])
        self.assertEqual(response['status_code'], 200)

        response_data = response['data']
        self.assertEqual(response_data['nama_lengkap'], "Salman Ganteng")
        self.assertNotEqual(response_data['nama_lengkap'], old_name)
        self.assertEqual(response_data['no_telp'], "081234567890")
        self.assertNotEqual(response_data['no_telp'], old_handphone_num)

    @mock.patch('simpk.bpkh_api.requests.get')
    def test_put_user_with_invalid_data_returns_exception(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        response_token = self.client.post(
            "http://localhost:8000/account/login",
            {'email': self.user_data['email'],
                'password': self.user_data['password']}
        ).json()['token']

        auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

        self.user_data_put_test['email'] = "salman"
        self.user_data_put_test['nama_lengkap'] = "Salman Ganteng"
        self.user_data_put_test['no_telp'] = "081234587678678667890"

        response = self.client.put(
            "http://localhost:8000/account/user",
            self.user_data_put_test,
            headers=auth_token
        ).json()

        self.assertFalse(response['success'])
        self.assertEqual(response['status_code'], 400)

        response_errors = response['errors']
        self.assertEqual(response_errors['email'], [
                         "Enter a valid email address."])
        self.assertEqual(response_errors['no_telp'], [
                         "Ensure this field has no more than 15 characters."])

    @mock.patch('simpk.bpkh_api.requests.get')
    def test_serializer_invalid_user(self, mock_requests):
        mock_requests.return_value.content = '{"result": "ERROR", "data": {}}'
        invalid_user_data = {
            'email': 'dummy@dummy.com',
            'password': 'dev-simpk-bpkh'
        }

        response = self.client.post(
            "http://localhost:8000/account/login", invalid_user_data)
        self.assertEqual(response.json()["non_field_errors"][0],
                         "A user with this email and password is not found.")

    def test_get_bpkh_email_internal(self):
        user =User.objects.get(pk=3)
        self.assertEqual(user.get_bpkh_email(), user.email)

    def test_get_bpkh_email_professional(self):
        user =User.objects.get(pk=1)
        self.assertEqual(user.get_bpkh_email(), "zona.ariemenda@bpkh.go.id")


class UserModelNegativeTest(TestCase):
    def test_user_register_has_no_email(self):
        to_be_registered = {
            'email': '',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }
        with self.assertRaises(ValueError) as error:
            User.objects.create_user(**to_be_registered)

    def test_user_register_has_no_password(self):
        to_be_registered = {
            'email': 'dummy@bpkh.go.id',
            'password': '',
            'nama_lengkap': 'Dummy',
            'no_telp': '0812XXXXXXXXX',
            'role': 'Test',
            'is_staff': True
        }
        with self.assertRaises(ValueError) as error:
            User.objects.create_user(**to_be_registered)

    def test_superuser_register_has_no_password(self):
        to_be_registered = {
            'email': 'dummy@bpkh.go.id',
            'nama_lengkap': 'Dummy',
            'no_telp': '0812XXXXXXXXX',
            'role': 'Test Super User'
        }
        with self.assertRaises(TypeError) as error:
            User.objects.create_superuser(**to_be_registered)

class UserPasswordChangeTest(TestCase):
    def setUp(self):
        self.client = RequestsClient()

        staf_data = {
            'email': 'zona.ariemenda@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)
        self.user_obj = User.objects.filter(pk=1).first()

        self.post_data = {
            "prev_password": "staffbpkh",
            "new_password": "nasigoreng",
            "confirm_password": "nasigoreng"
        }

        response_token = self.client.post(
            "http://localhost:8000/account/login",
            {'email': 'zona.ariemenda@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_change_password_should_change_password(self):
        response = self.client.post("http://localhost:8000/account/change-password",
            self.post_data, headers=self.auth_token)
        self.assertEqual(response.json()["password_changed"], True)
        self.assertEqual(response.status_code, 200)

    def test_when_old_password_is_not_match(self):
        self.post_data["prev_password"] = "halo"

        response = self.client.post("http://localhost:8000/account/change-password",
            self.post_data, headers=self.auth_token)
        self.assertEqual(response.json()["password_changed"], False)        

    def test_when_password_confirm_is_not_match(self):
        self.post_data["confirm_password"] = "halo"

        response = self.client.post("http://localhost:8000/account/change-password",
            self.post_data, headers=self.auth_token)
        self.assertEqual(response.json()["password_changed"], False)       

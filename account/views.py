from rest_framework import status
from rest_framework.generics import RetrieveAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from .serializers import UserLoginSerializer, UserPutSerializer
from .models import User

class UserLoginView(RetrieveAPIView):

    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'User logged in successfully',
            'token': serializer.data['token'],
            'role': serializer.data['role'],
        }
        status_code = status.HTTP_200_OK

        return Response(response, status=status_code)


class UserDataView(RetrieveAPIView):

    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request):
        user_obj = User.objects.get(email=request.user.email)
        status_code = status.HTTP_200_OK
        response = {
            'success': 'true',
            'status code': status_code,
            'message': 'User fetched successfully',
            'data': [{
                'email': user_obj.email,
                'nama_lengkap': user_obj.nama_lengkap,
                'no_telp': user_obj.no_telp,
                'role': user_obj.role,
            }]
        }
        return Response(response, status=status_code)

    def put(self, request):
        user_obj = User.objects.get(email=request.user.email)
        serializer = UserPutSerializer(user_obj, data=request.data)

        response = {}
        if serializer.is_valid():
            serializer.save()
            response['success'] = True
            response['status_code'] = status.HTTP_200_OK
            response['data'] = serializer.data
        else:
            response['success'] = False
            response['status_code'] = status.HTTP_400_BAD_REQUEST
            response['errors'] = serializer.errors

        return Response(response, status=response['status_code'])

class UserPasswordChange(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def post(self, request):
        user = request.user

        response = {}
        response["password_changed"] = False

        if user.check_password(request.data["prev_password"]):
            new_password = request.data["new_password"]
            confirm_password = request.data["confirm_password"]

            if new_password == confirm_password:
                user.set_password(new_password)
                user.save()

                response["password_changed"] = True
                response["message"] = "Password changed."

                return Response(data=response, status=status.HTTP_200_OK)
            else:
                response["message"] = "New password and confirmation password are not match."

                return Response(data=response, status=status.HTTP_200_OK)

        response["message"] = "Previous password is wrong."

        return Response(data=response, status=status.HTTP_200_OK)

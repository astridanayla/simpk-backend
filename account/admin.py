from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.safestring import mark_safe
from account.models import User, UserManager

User = get_user_model()

# Register your models here.
class UserAdmin(BaseUserAdmin):
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('nama_lengkap','email', 'admin', 'no_telp', 'role')
    list_filter = ('email', 'admin', 'staff', 'active')
    fieldsets = (
        (None, {'fields': ('nama_lengkap', 'email', 'password','no_telp','role')}),
        ('Permissions', {'fields': ('admin', 'staff', 'active',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('nama_lengkap','email', 'password1', 'password2', 'no_telp', 'role')}
        ),('Permissions', {'fields': ('admin', 'staff', 'active',)}),
    )
    search_fields = ('email', 'nama_lengkap',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(User, UserAdmin)

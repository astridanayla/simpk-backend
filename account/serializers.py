from django.contrib.auth.models import update_last_login
from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from simpk.bpkh_api import get_person
from account.models import User

JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('email', 'nama_lengkap', 'no_telp', 'role', 'password')
        extra_kwargs = {'password': {'write_only': True}}


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)
    role = serializers.CharField(max_length=15, read_only=True)

    def validate(self, data):
        email = data.get("email", None)
        password = data.get("password", None)
        is_email_password_not_found = False

        # check email exist in our own backend
        user = authenticate(email=email, password=password)
        user_exists = User.objects.filter(email=email).exists()

        if user_exists:
            user = authenticate(email=email, password=password)
            if user is None:
                is_email_password_not_found = True
        else:
            # check email exist in bpkh
            person = get_person(email)
            if person.get('result') == 'OK':
                default_password = 'BpkhDefault2021'
                person_data = person['data']

                no_telp = person_data['mobile']
                if no_telp is None:
                    no_telp = '0'
                user = User(
                    email=person_data['email'],
                    nama_lengkap=person_data['name'],
                    no_telp=no_telp,
                    role='Internal',
                    bpkh_id=person_data['personId'],
                    bpkh_token=person_data['token'],
                )
                user.set_password(default_password)
                user.save()

                user = authenticate(email=email, password=password)
                if user is None:
                    is_email_password_not_found = True
            else:
                is_email_password_not_found = True

        if is_email_password_not_found:
            raise serializers.ValidationError(
                'A user with this email and password is not found.'
            )

        payload = JWT_PAYLOAD_HANDLER(user)
        jwt_token = JWT_ENCODE_HANDLER(payload)
        update_last_login(None, user)

        return {
            'email': user.email,
            'token': jwt_token,
            'role': user.role,
        }


class UserPutSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=55)
    nama_lengkap = serializers.CharField(max_length=255)
    no_telp = serializers.CharField(max_length=15)

    def update(self, instance, data):
        instance.email = data.get('email', instance.email)
        instance.nama_lengkap = data.get('nama_lengkap', instance.nama_lengkap)
        instance.no_telp = data.get('no_telp', instance.no_telp)
        instance.save()
        return instance

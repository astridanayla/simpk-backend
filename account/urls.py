from django.urls import path
from .views import UserLoginView, UserDataView, UserPasswordChange

urlpatterns = [
    path('login', UserLoginView.as_view()),
    path('user', UserDataView.as_view()),
    path('change-password', UserPasswordChange.as_view())
]

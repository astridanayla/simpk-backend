from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self, email, nama_lengkap=None, no_telp=None, role=None, password=None,
        is_active=True, is_staff=False, is_admin=False):

        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")

        user_obj = self.model(
            email=self.normalize_email(email),
            nama_lengkap=nama_lengkap,
            role=role,
            no_telp=no_telp,
        )

        user_obj.set_password(password) # change user password
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self, email, nama_lengkap=None, no_telp=None, role=None, password=None):
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(
                email,
                nama_lengkap=nama_lengkap,
                no_telp=no_telp,
                role=role,
                password=password,
                is_staff=True,
                is_admin=True

        )
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    ROLE = (
        ('Admin', 'Admin'),
        ('Internal', 'Internal'),
        ('Professional', 'Professional'),
        ('Mitra', 'Mitra')
    )


    email               = models.EmailField(max_length=55, unique=True)
    nama_lengkap        = models.CharField(max_length=55)
    no_telp             = models.CharField(max_length=15)
    role                = models.CharField(max_length=15, choices=ROLE)
    active              = models.BooleanField(default=True) # can login
    staff               = models.BooleanField(default=False) # staff user non superuser
    admin               = models.BooleanField(default=False) # superuser
    timestamp           = models.DateTimeField(auto_now_add=True)
    bpkh_id             = models.IntegerField(null=True)
    bpkh_token          = models.CharField(max_length=50, blank=True)
    mitra_id            = models.IntegerField(blank=True, null=True)
    proyek              = models.ManyToManyField(to="monitoring.Proyek")

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['nama_lengkap', 'no_telp', 'role']

    objects = UserManager()

    def __str__(self): # pragma: no cover
        return self.nama_lengkap

    def has_perm(self, perm, obj=None): # pragma: no cover
        return True

    def has_module_perms(self, app_label): # pragma: no cover
        return True

    def get_bpkh_email(self):
        if self.role == "Mitra" or self.role == "Professional":
            return "zona.ariemenda@bpkh.go.id"
        return self.email

    @property
    def is_staff(self): # pragma: no cover
        return self.staff

    @property
    def is_admin(self): # pragma: no cover
        return self.admin

    @property
    def is_active(self):
        return self.active

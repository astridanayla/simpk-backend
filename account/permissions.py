from rest_framework import permissions


class ProfessionalReadOnlyPermission(permissions.BasePermission):
    """
    allow if request.user.role is not "Professional"
    """

    def has_permission(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True
        return request.user.role != 'Professional'

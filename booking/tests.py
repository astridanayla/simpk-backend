import datetime
from unittest import mock

from django.apps import apps
from django.db import transaction
from django.test import TestCase
from rest_framework.test import RequestsClient

from account.models import User
from booking.apps import BookingConfig
from booking.models import Booking
from booking.serializers import BookingSerializer
from monitoring.models import Proyek


class BookingUnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(BookingConfig.name, "booking")
        self.assertEqual(apps.get_app_config("booking").name, "booking")


class BookingModelTest(TestCase):
    """This class defines the test suite for the booking model."""

    def setUp(self):
        """Define the test client and other test variables."""

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        User.objects.create_user(**staf_data)

        self.user_obj = User.objects.filter(pk=1).first()

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.proyek_obj.save()

        self.booking_obj = Booking(nama_kegiatan="Observasi",
                                   waktu_booking=datetime.time(15, 0, 0), tanggal_booking=datetime.date(2020, 4, 12),
                                   proyek=self.proyek_obj, user=self.user_obj, deskripsi="Tatang")

    def test_create_new_booking_object_count_incremented(self):
        """Test the booking model when new object created, count should be incremented"""

        current_booking_count = Booking.objects.count()
        self.booking_obj.save()
        new_booking_count = Booking.objects.count()
        self.assertNotEqual(current_booking_count, new_booking_count)

    def test_when_input_invalid_booking_data_should_not_create_booking(self):
        booking_data = {
            "nama_kegiatan": "Observasi",
            "waktu_booking": "09:00:00",
            "tanggal_booking": "2020-04-12",
            "proyek": self.proyek_obj,
            "user": "",
            "deskripsi": "Tatang"
        }

        try:
            invalid_booking = Booking(**booking_data)
        except ValueError:
            self.assertEqual(Booking.objects.count(), 0)


class BookingSerializerTest(TestCase):
    def setUp(self):

        self.proyek_obj = Proyek(project_id=1,
                                 koordinat_lokasi="(-6.310016735496198, 106.82030167073002)",
                                 deskripsi="Lorem ipsum dolor sit amet",
                                 progres="60")

        self.booking_data = {
            "nama_kegiatan": "Observasi",
            "waktu_booking": "09:00:00",
            "tanggal_booking": "2020-04-12",
            "proyek": self.proyek_obj,
            "deskripsi": "Tatang"
        }

        self.booking_obj = Booking(**self.booking_data)

        self.booking_serializer = BookingSerializer(instance=self.booking_obj)

    def test_booking_serialier_contains_field(self):
        data = self.booking_serializer.data
        self.assertEqual(
            set(data.keys()),
            set(["id", "nama_kegiatan", "waktu_booking", "tanggal_booking",
                 "proyek", "user", "deskripsi", "user_name"]))


class RESTAPIBookingByProyekTest(TestCase):
    @mock.patch('simpk.bpkh_api.requests.get')
    def setUp(self, mock_requests):
        mock_requests.return_value.content = '{"result": "OK", "data": {"personId": 1, "token": "asdf"}}'
        self.client = RequestsClient()
        self.url_index = "http://localhost:8000/booking-app/by-proyek/1"
        self.url_detail = "http://localhost:8000/booking-app/by-proyek/1/booking/1"

        staf_data = {
            'email': 'stafbpkh@bpkh.go.id',
            'password': 'staffbpkh',
            'nama_lengkap': 'Staf BPKH',
            'no_telp': '081356979643',
            'role': 'Internal',
            'is_staff': True
        }

        user = User.objects.create_user(**staf_data)

        proyek_data = {
            "project_id": 1,
            "koordinat_lokasi": "(-6.310016735496198, 106.82030167073002)",
            "deskripsi": "Lorem ipsum dolor sit amet",
            "progres": "60"
        }

        proyek_obj = Proyek(**proyek_data)
        proyek_obj.save()

        self.booking_data = {
            "nama_kegiatan": "Observasi",
            "waktu_booking": "09:00:00",
            "tanggal_booking": "2020-04-12",
            "proyek": proyek_obj,
            "user": User.objects.filter(pk=1).first(),
            "deskripsi": "Tatang"
        }

        self.result_data = {
            "id": 1,
            "nama_kegiatan": "Observasi",
            "waktu_booking": "09:00:00",
            "tanggal_booking": "2020-04-12",
            "proyek": 1,
            "user": 1,
            "user_name": "Staf BPKH",
            "deskripsi": "Tatang"
        }

        self.sended_data = {
            "id": 2,
            "nama_kegiatan": "Observasi",
            "waktu_booking": "09:00:00",
            "tanggal_booking": "2020-04-12",
            "proyek": 1,
            "user": 1,
            "user_name": "Staf BPKH",
            "deskripsi": "Tatang"
        }

        Booking(**self.booking_data).save()

        self.booking_data["proyek"] = 1
        self.booking_data["user"] = 1

        response_token = self.client.post("http://localhost:8000/account/login",
                                          {'email': 'stafbpkh@bpkh.go.id', 'password': 'staffbpkh'}).json()['token']

        self.auth_token = {
            'Authorization': 'Bearer ' + response_token
        }

    def test_url_booking_api_is_exist(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_post_booking_with_wrong_format_returns_http_400(self):
        self.booking_data["waktu_booking"] = "Kabinet"

        response = self.client.post(
            self.url_index, self.booking_data, headers=self.auth_token)
        self.assertEqual(response.status_code, 400)

    def test_get_booking_return_json_booking_data(self):
        response = self.client.get(self.url_index, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)

    def test_post_booking_return_json_that_sended(self):
        response = self.client.post(
            self.url_index, self.booking_data, headers=self.auth_token)
        self.assertEqual(response.json(), self.sended_data)

    def test_get_booking_by_id_return_json(self):
        response = self.client.get(self.url_detail, headers=self.auth_token)
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0], self.result_data)

    def test_delete_booking_by_id_return_204(self):
        response = self.client.delete(self.url_detail, headers=self.auth_token)
        self.assertEqual(response.status_code, 204)

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from booking.models import Booking
from booking.serializers import BookingSerializer
from monitoring.models import Proyek

class BookingFilterByProyekIndex(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def post(self, request, proyek_pk):
        to_be_post = request.data.copy()
        to_be_post["user"] = request.user.id
        to_be_post["proyek"] = proyek_pk
        serializer = BookingSerializer(data=to_be_post)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, proyek_pk):
        proyek = Proyek.objects.filter(pk=proyek_pk).first()
        booking_list = Booking.objects.filter(proyek=proyek)
        serializer = BookingSerializer(booking_list, many=True)
        return Response(serializer.data)

class BookingFilterByProyekDetail(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def get(self, request, proyek_pk, booking_pk):
        proyek = Proyek.objects.filter(pk=proyek_pk).first()
        booking_obj = Booking.objects.filter(pk=booking_pk, proyek=proyek)
        serializer = BookingSerializer(booking_obj, many=True)
        return Response(serializer.data)

    def delete(self, request, proyek_pk, booking_pk):
        proyek = Proyek.objects.filter(pk=proyek_pk).first()
        Booking.objects.filter(pk=booking_pk, proyek=proyek).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

from django.urls import path
from booking.views import (BookingFilterByProyekDetail,
    BookingFilterByProyekIndex)

urlpatterns = [
    path('by-proyek/<proyek_pk>', BookingFilterByProyekIndex.as_view(),
        name="booking_proyek_index"),
    path('by-proyek/<proyek_pk>/booking/<booking_pk>', BookingFilterByProyekDetail.as_view(),
        name="booking_proyek_detail"),
]

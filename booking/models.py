from django.db import models
from monitoring.models import Proyek
from account.models import User

class Booking(models.Model):
    nama_kegiatan = models.CharField(max_length=30)
    waktu_booking = models.TimeField()
    tanggal_booking = models.DateField()
    proyek = models.ForeignKey(Proyek, to_field='project_id', on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    deskripsi = models.TextField(null=True)

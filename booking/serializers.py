from rest_framework import serializers
from booking.models import Booking


class BookingSerializer(serializers.ModelSerializer):
    """Define the booking serializer class"""
    user_name = serializers.CharField(source='user.nama_lengkap', read_only=True)

    class Meta:
        """Meta class for Booking Model"""

        model = Booking
        fields = ('id', 'nama_kegiatan', 'waktu_booking', 'tanggal_booking',
                  'deskripsi', 'proyek', 'user', 'user_name')
